import React, { useEffect, useState, createContext } from "react";
import axios from 'axios'

export const MovieContext = createContext();
export const MovieProvider = props => {

  const [movie, setMovie] = useState([]);
  
  // untuk form
  const [id, setId] = useState("")
  const [title, setTitle] = useState("")
  const [description, setDescription] = useState("")
  const [year, setYear] = useState(2020)
  const [duration, setDuration] = useState(120)
  const [genre, setGenre] = useState("")
  const [rating, setRating] = useState(0)
  const [image, setImage] = useState("")


  // untuk login
  const [islogin, setIslogin] = useState(false)
  const [username, setUsername] = useState("")
  const [password, setPassword] = useState("")


  const setLogin = async (status) => {
    setIslogin(status);
  };

  const fetchData = async () => {
    const result = await axios(
      'http://backendexample.sanbercloud.com/api/movies',
    );
    console.log(result.data);
    setMovie(result.data);
  };
  
  const addData = async (item) => {
    const result = await axios.post(
      'http://backendexample.sanbercloud.com/api/movies',
      item
    );
  };
  const updData = async (item) => {
    const result = await axios.put(
      'http://backendexample.sanbercloud.com/api/movies/' + id,
      item
    );
  };
  const delData = async (item) => {
    const result = await axios.delete(
      'http://backendexample.sanbercloud.com/api/movies/' + item.id,
    );
  };

  // useEffect(() => {
  //   fetchData();
  // }, []);

  return (
    <MovieContext.Provider value={[
        movie, setMovie,  fetchData, addData, updData, delData,
        setLogin, islogin, setIslogin, username, setUsername, password, setPassword,
        id, setId, title, setTitle, description, setDescription, year, setYear,
        duration, setDuration, genre, setGenre, rating, setRating, image, setImage
      ]}>
      {props.children}
    </MovieContext.Provider>
  );
};

// export default MovieProvider;