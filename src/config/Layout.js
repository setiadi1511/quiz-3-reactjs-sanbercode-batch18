import React from 'react';
import { BrowserRouter as Router } from "react-router-dom";
import './../App.css';
import Nav from "../component/Nav";
import Routes from '../config/Routes';
import {MovieProvider} from "./../config/MovieContext";


function Layout() {
  return (
    <MovieProvider>
      <Router>
        <Nav></Nav>
        <br/>
        <div className="body">
          <Routes/>
        </div>
      </Router>   
    </MovieProvider>
    );
  }

export default Layout;
