import React, { useEffect, useContext, useState, createContext } from 'react';
import { MovieContext } from "./../config/MovieContext"
import { Switch, Route } from "react-router";
import Home from "../views/Home";
import About from "../views/About";
import Movie from "../views/Movie";
import Login from "../views/Login";
const Routes = () => {
  const [
    movie, setMovie,  fetchData, addData, updData, delData,
    setLogin, islogin, setIslogin, username, setUsername, password, setPassword,
    id, setId, title, setTitle, description, setDescription, year, setYear,
    duration, setDuration, genre, setGenre, rating, setRating, image, setImage
  ] = useContext(MovieContext);
  return (
    <Switch>
      <Route exact path="/">
        <Home />
      </Route>
      <Route exact path="/about">
        <About />
      </Route>
      {islogin && (
        <Route exact path="/movie">
          <Movie />
        </Route>
      )}
      {!islogin && (
        <Route exact path="/login">
          <Login />
        </Route>
      )}
    </Switch>
  );
};

export default Routes;