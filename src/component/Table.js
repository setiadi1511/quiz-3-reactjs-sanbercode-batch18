import React from 'react';

  function Table(props) {
    var head = Object.keys(props.data[0]);
    return (
      <div>
        <h2>{props.label}</h2>
        <table>
          {props.head && (
            <thead>
              <tr>
                {head.map((th,index) => 
                  <th key={index}>{th}</th>  
                )}
              </tr>
            </thead>
          )}
          <tbody>
            {props.data.map((data,index) => 
              <tr key={index}>
                {head.map((td,ind) => 
                  <td key={ind}>{data[td]}</td>  
                )}
              </tr>
            )}
          </tbody>
        </table>
      </div>
    )
  }

  export default Table;