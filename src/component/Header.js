import React from 'react';

function Header() {
  return (
    <>
      <header>
        <img id="logo" src="./../public/img/logo.png" width="200px" />
        <nav>
          <ul>
            <li><a href="index.html">Home </a> </li>
            <li><a href="about.html">About </a> </li>
            <li><a href="contact.html">Contact </a> </li>
          </ul>
        </nav>
      </header>   
    </>
    );
  }

export default Header;
