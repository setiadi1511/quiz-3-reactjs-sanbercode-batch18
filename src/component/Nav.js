import React, { useEffect, useContext, useState, createContext } from 'react';
import { MovieContext } from "./../config/MovieContext"
import { NavLink as Link } from "react-router-dom";
import './../App.css';


const Nav = () => {
  const [
    movie, setMovie,  fetchData, addData, updData, delData,
    setLogin, islogin, setIslogin, username, setUsername, password, setPassword,
    id, setId, title, setTitle, description, setDescription, year, setYear,
    duration, setDuration, genre, setGenre, rating, setRating, image, setImage
  ] = useContext(MovieContext);

  
  const logout = async (e) => {
    e.preventDefault();
    setLogin(false);
  }
  return (
    <div>
      <div><img className="nav-img" src="./../img/logo.png" alt="" /></div>
      <div className="nav">
        <ul>
          <li>
            <Link to="/" exact={true}>Home</Link>
          </li>
          <li>
            <Link to="/about">About</Link>
          </li>
          {!islogin && (
            <li>
              <Link to="/login">Login</Link>
            </li>
          )}
          {islogin && (
            <li>
              <Link to="/Movie">Movie</Link>
            </li>
          )}
          {islogin && (
            <li>
              <Link onClick={logout} to="/logout">Logout</Link>
            </li>
          )}
        </ul>
      </div>
    </div>
  )
}

export default Nav