import React, { useEffect, useContext, useState, createContext } from 'react';
import { MovieContext } from "./../config/MovieContext"

function Movie() {
  const [
    movie, setMovie, fetchData, addData, updData, delData,
    islogin, setIslogin, username, setUsername, password, setPassword,
    id, setId, title, setTitle, description, setDescription, year, setYear,
    duration, setDuration, genre, setGenre, rating, setRating, image, setImage
  ] = useContext(MovieContext);

  const handleForm = async (e) => {
    e.preventDefault()
    let item = {
      title: title,
      description: description,
      year: year,
      duration: duration,
      genre: genre,
      rating: rating,
      image_url: image,
    }
    if (id) {
      await updData(item);
    } else {
      await addData(item);
    }
    setId(0)
    setTitle('')
    setDescription('')
    setYear(2020)
    setDuration(120)
    setGenre('')
    setRating(0)
    setImage('')
    await fetchData();
  }

  const upd = async (item, index) => {

    setId(item.id)
    setTitle(item.title)
    setDescription(item.description)
    setYear(item.year)
    setDuration(item.duration)
    setGenre(item.genre)
    setRating(item.rating)
    setImage(item.image_url)
  }
  const del = async (item, index) => {
    await delData(item);
    await fetchData();
  }

  useEffect(() => {
    fetchData();
  }, []);
  return (
    <div className="container">
      <div className="movie">
        <div className="search">
          <input type="text" /><button>Search</button>
        </div>
        <h2>Daftar Movie</h2>
        <table className="list">
          <thead>
            <tr>
              <th>No</th>
              <th>Title</th>
              <th>Description</th>
              <th>Year</th>
              <th>Duration</th>
              <th>Genre</th>
              <th>Rating</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {movie.map((item, index) => {
              return (
                <tr key={index}>
                  <td>{index + 1}</td>
                  <td>{item.title}</td>
                  <td className="maxline">{item.description}</td>
                  <td>{item.year}</td>
                  <td>{item.duration}</td>
                  <td>{item.genre}</td>
                  <td>{item.rating}</td>
                  <td><button onClick={upd.bind(this, item)}>Edit</button><button onClick={del.bind(this, item)}>Delete</button></td>
                </tr>
              )
            })}
          </tbody>
        </table>
        <br />
        <br />
        <h2>Movie Form</h2>
        <form onSubmit={handleForm}>
          <table className="form">
            <tbody>
              <tr>
                <td>Title:</td>
                <td><input type="text" value={title} onChange={e => setTitle(e.target.value)} /></td>
              </tr>
              <tr>
                <td>Description:</td>
                <td><textarea value={description} onChange={e => setDescription(e.target.value)}></textarea></td>
              </tr>
              <tr>
                <td>Year:</td>
                <td><input type="number" value={year} onChange={e => {
                  if (e.target.value >= 1980) {
                    setYear(e.target.value)
                  }
                }} /></td>
              </tr>
              <tr>
                <td>Duration:</td>
                <td><input type="number" value={duration} onChange={e => setDuration(e.target.value)} /></td>
              </tr>
              <tr>
                <td>Genre:</td>
                <td><input type="text" value={genre} onChange={e => setGenre(e.target.value)} /></td>
              </tr>
              <tr>
                <td>Rating:</td>
                <td><input type="number" value={rating} onChange={e => {
                  if (e.target.value >= 0 && e.target.value <= 10) {
                    setRating(parseInt(e.target.value))
                  }
                }} /></td>
              </tr>
              <tr>
                <td>Image Url:</td>
                <td><textarea value={image} onChange={e => setImage(e.target.value)}></textarea></td>
              </tr>
              <tr>
                <td></td>
                <td><button>Submit</button></td>
              </tr>
            </tbody>
          </table>
        </form>
      </div>
    </div>
  );
}

export default Movie;
