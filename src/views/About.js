import React, { useEffect, useState, createContext } from 'react';
import Table from "./../component/Table";

function About(prop) {
  const [data, setData] = useState([
    {no:1,item:"Nama: Dwi Setiadi Nugroho"},
    {no:2,item:"Email: setiadi1511@gmail.com"},
    {no:3,item:"Sistem Operasi yang digunakan: Windows 10"},
    {no:4,item:"Akun Gitlab: https://gitlab.com/setiadi1511"},
    {no:5,item:"Akun Telegram: +628818082948"},
  ]);
  return (
    <>
      <div id="about-page">
        <div className="container">
          <Table head={false} label="Data Peserta Sanbercode Bootcamp Reactjs" data={data} />
        </div>
      </div> 
    </>
    );
  }

export default About;
