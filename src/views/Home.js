import React, { useEffect, useContext, useState, createContext } from 'react';
import { MovieContext } from "./../config/MovieContext"
function Home() {
    const [
        movie, setMovie, fetchData, addData, updData, delData,
        setLogin, islogin, setIslogin, username, setUsername, password, setPassword,
        id, setId, title, setTitle, description, setDescription, year, setYear,
        duration, setDuration, genre, setGenre, rating, setRating, image, setImage
    ] = useContext(MovieContext)

    useEffect(() => {
        fetchData();
    }, []);
    return (
        <>
            <div className="container">
                <section >
                    <h1>Daftar Film Film Terbaik</h1>
                    <div id="article-list">
                        {movie.map((item, index) => {
                            return (
                                <div className="article" key={index}>
                                    <div>
                                        <img src={item.image_url} alt="" />
                                        <div className="info">
                                            <h3>{item.title}</h3>
                                            <div>Rating: {item.rating}</div>
                                            <div>Durasi: {(item.duration / 60).toFixed(2)} Jam</div>
                                            <div>genre : {item.genre}</div>
                                            <hr />
                                            <div>Description:</div>
                                            <div>{item.description}</div>
                                        </div>
                                    </div>
                                    <p></p>
                                </div>
                            )
                        })}
                    </div>
                </section>
            </div>
        </>
    );
}

export default Home;
