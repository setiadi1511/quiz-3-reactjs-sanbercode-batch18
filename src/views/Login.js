import React, { useEffect, useContext, useState } from 'react';
import { MovieContext } from "./../config/MovieContext"
function Login() {
  const [
    movie, setMovie, fetchData, addData, updData, delData,
    setLogin, islogin, setIslogin, username, setUsername, password, setPassword,
    id, setId, title, setTitle, description, setDescription, year, setYear,
    duration, setDuration, genre, setGenre, rating, setRating, image, setImage
  ] = useContext(MovieContext);

  const handleForm = async (e) => {
    e.preventDefault();

    if (username == 'admin' && password == 'admin') {
      setLogin(true);
      alert('Login Success...');
    } else {
      alert('Username and password not found!');
    }
  }

  return (
    <div className="login">
      <div className="container">
        <form className="box" onSubmit={handleForm}>
          <table>
            <tbody>
              <tr>
                <td>Username</td>
                <td>:</td>
                <td><input type="text" value={username} onChange={e => setUsername(e.target.value)} /></td>
              </tr>
              <tr>
                <td>Password</td>
                <td>:</td>
                <td><input type="password" value={password} onChange={e => setPassword(e.target.value)} /></td>
              </tr>
              <tr>
                <td></td>
                <td></td>
                <td><button>Submit</button></td>
              </tr>
              <tr>
                <td colSpan="3"><br /> username: "admin", password: "admin"</td>
              </tr>
            </tbody>
          </table>
        </form>
      </div>
    </div>
  );
}

export default Login;
